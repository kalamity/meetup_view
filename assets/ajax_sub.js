//------------------- Show Subscriber-------------------\\
$.ajax({
  url: "http://localhost/projets/meetup_api/subscribers",
  method: "GET"
}).done(function(json) {
  var json = JSON.parse(json);
  json.forEach(function(json) {
    $(".subscriber").append(`<li>${json.first_name} ${json.last_name}</li>`);
  });
});

//------------------- Add Subscriber-----------------------\\
$(".form_sub").on("submit", function(event) {
  event.preventDefault();
  var data = {
    first_name: $("#sub_first_name").val(),
    last_name: $("#sub_last_name").val(),
    mail_addr: $("#sub_mail_addr").val()
  };
  
  //Verfication de champs - eviter les champs vides
  var text = document.querySelectorAll(".check");
  if (text[0].value === "" || text[1].value === "" || text[2].value === "") {
    toastr.error("Merci de remplir tout les champs");
    event.preventDefault();
  } else {
    $.ajax({
      url: "http://localhost/projets/meetup_api/subscriber",
      method: "POST",
      data: data
    }).done(function(data) {
      var json = JSON.parse(data);
      $(".subscriber").append(`<li>${json.first_name} ${json.last_name}</li>`);
      toastr.success("Subscribeur-euse ajouté-e");
    });
  }
});
