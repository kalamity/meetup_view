<!DOCTYPE html>
<html lang="en">

<head>
<?php include('parts/head.php') ?>
</head>

<body>
    <h1 class="text-center display-4">Meetup</h1>
    <?php include('parts/nav.php') ?>
    <div class="row">
        <!--Affichage des datas-->
        <div class="col-6">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title text-center">Les Speakers</h5>
                    <ul class="speaker meetups card-text">
                    </ul>
                </div>
            </div>
        </div>
        <!--Add des datas-->
        <div class="col-6">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title text-center">Ajouter un.e speakeur.euse</h5>
                    <form class="form_speaker" id="speaker">
                        <div class="form-group">
                            <label>Prénom</label>
                            <input type="text" class="form-control check" id="spe_first_name" name="first_name">
                        </div>
                        <div class="form-group">
                            <label>Nom</label>
                            <input type="text" class="form-control check" id="spe_last_name" name="last_name">
                        </div>
                        <div class="form-group">
                            <label>Descritpion</label>
                            <textarea class="form-control check" name="description" id="spe_description" rows="2"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary" id="submit_speaker">Envoyer</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script src="assets/ajax_speak.js"></script>


</body>
</html>