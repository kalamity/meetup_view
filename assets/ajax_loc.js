//------------------- Show Location-------------------\\
$.ajax({
  url: "http://localhost/projets/meetup_api/locations",
  method: "GET"
}).done(function(json) {
  var json = JSON.parse(json);
  json.forEach(function(json) {
    $(".location").append(
      `<li><h6>${json.city}</h6><p>${
        json.description
      }</p>
      <p>${
        json.address
      }</p>
      <p>${
        json.zip_code
      }</p>
      </li>`
    );
  });
});

//------------------- Add Location-----------------------\\
$(".form_location").on("submit", function(event) {
  event.preventDefault();
  var data = {
    city: $("#loc_city").val(),
    address: $("#loc_address").val(),
    zip_code: $("#loc_zip_code").val(),
    description: $("#loc_description").val()
  };
  //Verfication de champs - eviter les champs vides
  var text = document.querySelectorAll(".check");
  console.log(data);
  if (
    text[0].value === "" ||
    text[1].value === "" ||
    text[2].value === "" ||
    text[3].value === ""
  ) {
    toastr.error("Merci de remplir tout les champs");
    event.preventDefault();
  } else {
    $.ajax({
      url: "http://localhost/projets/meetup_api/location",
      method: "POST",
      data: data
    }).done(function(data) {
      var json = JSON.parse(data);
      $(".location").append(
        `<li><h6>${json.city}</h6><p>${
          json.description
        }</p>
        <p>${
          json.address
        }</p>
        <p>${
          json.zip_code
        }</p>
        </li>`
      );
      toastr.success("Lieu ajouté");
    });
  }
});

//------------------- Delete Location-----------------------\\
$(document).on("click", ".delete", function(del) {
  var data = $(this)
    .parent()
    .parent(); //pour surpprimer le li et non le bouton dell
  $.ajax({
    url:
      "meetup_api/meetup/" + $(this).attr("data-id"),
    method: "DELETE"
  }).done(function(del) {
    data.remove(); // Supprime la ligne de meetup
    toastr.success("Meetup supprimé");
  });
});