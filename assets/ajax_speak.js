//------------------- Show Speaker-------------------\\
$.ajax({
  url: "http://localhost/projets/meetup_api/speakers",
  method: "GET"
}).done(function(json) {
  var json = JSON.parse(json);
  json.forEach(function(json) {
    $(".speaker").append(
      `<li><h6>${json.first_name} ${json.last_name}</h6>
      <p>${json.description}</p></li>`
    );
  });
});

//------------------- Add Speaker-------------------\\
$(".form_speaker").on("submit", function(event) {
  event.preventDefault();
  var data = {
    first_name: $("#spe_first_name").val(),
    last_name: $("#spe_last_name").val(),
    description: $("#spe_description").val()
  };
  //Verfication de champs - eviter les champs vides
  var text = document.querySelectorAll(".check");
  console.log(data);
  if (text[0].value === "" || text[1].value === "" || text[2].value === "") {
    toastr.error("Merci de remplir tout les champs");
    event.preventDefault();
  } else {
    $.ajax({
      url: "http://localhost/projets/meetup_api/speaker",
      method: "POST",
      data: data
    }).done(function(data) {
      var json = JSON.parse(data);
      console.log(json)
      $(".speaker").append(
        `<li><h6>${json.first_name} ${json.last_name}</h6>
        <p>${json.description}</p></li>`
      );
      toastr.success("Speaker-euse ajouté-e");
    });
  }
});
