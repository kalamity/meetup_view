<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!--Style Link (font,css)-->
    <link rel="stylesheet" href="assets/style.css">
    <!--Bootstrap Script-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <!--JS-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!--Toastr-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet" />

    <title>Meetup</title>
</head>

<body>
    <h1 class="text-center display-4">Meetup</h1>
    <?php include('parts/nav.php') ?>
    <div class="row">
        <!--Affichage des datas-->
        <div class="col-6">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Les Participant.e.s</h5>
                    <ul class="subscriber card-text">
                    </ul>
                </div>
            </div>
        </div>
        <!--Ajout des datas-->
        <div class="col-6">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title text-center">Ajouter un.e participant.e</h5>
                    <form class="form_sub" id="subscriber">
                        <div class="form-group">
                            <label>Prénom</label>
                            <input type="text" class="form-control check" id="sub_first_name" name="first_name">
                        </div>
                        <div class="form-group">
                            <label>Nom</label>
                            <input type="text" class="form-control check" id="sub_last_name" name="last_name">
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" class="form-control check" id="sub_mail_addr" name="mail_addr">
                        </div>
                        <button type="submit" class="btn btn-primary" id="submit_subscriber">Envoyer</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script src="assets/ajax_sub.js"></script>

</body>

</html>