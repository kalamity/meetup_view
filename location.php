<!DOCTYPE html>
<html lang="en">
<head>
<?php include('parts/head.php') ?>
</head>
<body>
    <h1 class="text-center display-4">Meetup</h1>
    <?php include('parts/nav.php') ?>
    <div class="row">
        <!--Affichage des datas-->
        <div class="col-6">
            <div class="card">
                <div class="card-body ">
                    <h5 class="card-title text-center">Les Lieux</h5>
                    <ul class="meetups location card-text">
                    </ul>
                </div>
            </div>
        </div>
        <!--Form-->
        <div class="col-6">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title text-center">Ajouter un lieu</h5>
                    <form class="form_location" novalidate>
                        <div class="form-group">
                            <label>Ville</label>
                            <input type="text" class="form-control check" id="loc_city" name="city" required>
                        </div>
                        <div class="form-group">
                            <label>Adresse</tlabel>
                                <input type="text" class="form-control check" id="loc_address" name="address" required>
                        </div>
                        <div class="form-group">
                            <label>Code postal</tlabel>
                                <input type="text" class="form-control check" id="loc_zip_code" name="zip_code" required>
                        </div>
                        <div class="form-group">
                            <label>Descritpion</label>
                            <textarea class="form-control check" name="description" id="loc_description" rows="2" required></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary" id="submit_loc">Envoyer</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script src="assets/ajax_loc.js"></script>
</body>

</html>