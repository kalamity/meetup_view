//------------------- Show Meetups-------------------\\
$.ajax({
  url: "http://localhost/projets/meetup_api/meetups",
  method: "GET"
}).done(function(json) {
  var json = JSON.parse(json);
  json.forEach(function(json) {
    $(".meetups").append(
      `<li><h6 class="one_meetup" data-id="${json["id"]}">${
        json.title
      }</h6><span><i class="delete fas fa-trash-alt" data-id="${
        json["id"]
      }"></i></span></li>`
    );
  });
});

//------------------- Show Locations On Meetup-------------------\\
$.ajax({
  url: "http://localhost/projets/meetup_api/locations",
  method: "GET"
}).done(function(json) {
  var json = JSON.parse(json);
  json.forEach(function(json) {
    $(".location").append(
      `<option>${json.city}</option>`
    );
  });
});

//------------------- Show Speakers On Meetup-------------------\\
$.ajax({
  url: "http://localhost/projets/meetup_api/speakers",
  method: "GET"
}).done(function(json) {
  var json = JSON.parse(json);
  json.forEach(function(json) {
    $(".speaker").append(
      `<option>${json.first_name} ${json.last_name}</</option>`
    );
  });
});
//---------Add Meetup------\\
$(".form_meetup").on("submit", function(event) {
  event.preventDefault();
  var data = {
    title: $("[name=title]").val(),
    title: $("[name=speaker]").val(),
    description: $("[name=description]").val(),
    location: $("[name=location]").val(),
    image: $("[name=image]").val(),
    date: $("[name=date]").val()
  };
  var text = document.querySelectorAll(".check");
  console.log(data);
  if (
    text[0].value === "" ||
    text[1].value === "" ||
    text[2].value === ""
  ) {
    toastr.error("Merci de remplir tout les champs");
  } else {
    $.ajax({
      url: "http://localhost/projets/meetup_api/meetup",
      type: "POST",
      data: data
    }).done(function(data) {
        var json = JSON.parse(data);
        $(".meetups").append(
          `<li><h6 class="one_meetup" data-id="${json["id"]}">${
            json.title
          }</h6><span><i class="delete fas fa-trash-alt" data-id="${
            json["id"]
          }"></i></span></li>`
        );
      })
      .done(function() {
        $("#id").val(``); //clear form
        $("#title").val(``);
        $("#description").val(``);
        $("#image").val(``);
        $("#date").val(``);
        toastr.success("Meetup ajouté");
      });
  }
});

//---------Delete Meetup------\\
$(document).on("click", ".delete", function(del) {
  var data = $(this)
    .parent()
    .parent(); //pour surpprimer le li et non le bouton dell
  $.ajax({
    url:
      "http://localhost/projets/meetup_api/meetup/" + $(this).attr("data-id"),
    method: "DELETE"
  }).done(function(del) {
    data.remove(); // Supprime la ligne de meetup
    $("#meetup").html(""); //Suprime la vue détaillé
    toastr.success("Meetup supprimé");
  });
});

//---------Show One Meetup ------\\
$(document).on("click", ".one_meetup", function() {
  $.ajax({
    url:
      "http://localhost/projets/meetup_api/meetup/" + $(this).attr("data-id"),
    method: "GET"
  }).done(function(json) {
    //Show in the Meetup Div
    var json = JSON.parse(json);
    $("#meetup").html("");
    $("#meetup").append(
      //Display the meetup info
      `
      <h5>${json.title}</h5>
      <h6>${json.speaker}</h6>
      <ul>
      <li>${json.description}</li>
      <li>${json.location}</li>
      <li>${json.date}</li>
      </ul>
      <span><img src="${json.image}" class="img-thumbnail rounded"></span>`
    );
    $("#id_up").val(`${json.id}`); //Show in the Modifier Meetup form
    $("#title_up").val(`${json.title}`);
    $("#description_up").val(`${json.description}`);
    $("#location_up").val(`${json.location}`);
    $("#image_up").val(`${json.image}`);
    $("#date_up").val(`${json.date}`);
  });
});

// ---------Update Meetup---------\\
$(document).on("click", ".update", function() {
  //Modify the Meetup Form
  var id = $("[name=id_up]").val();
  var data = {
    id: $("[name=id_up]").val(),
    title: $("[name=title_up]").val(),
    speaker: $("[name=speaker_up]").val(),
    description: $("[name=description_up]").val(),
    location: $("[name=location_up]").val(),
    image: $("[name=image_up]").val(),
    date: $("[name=date_up]").val()
  };
  var text = document.querySelectorAll(".checkUpdate");
  if (text[0].value === "" || text[1].value === "") {
    toastr.error("Merci de remplir tout les champs");
    event.preventDefault();
  } else {
    $.ajax({
      url: "http://localhost/projets/meetup_api/meetup/" + id,
      method: "PUT",
      data: data
    }).done(function() {
      $("#id_up").val(``); //clear form
      $("#title_up").val(``);
      $("#description_up").val(``);
      $("#image_up").val(`${json.image}`);
      $("#date_up").val(`${json.date}`);
    });
    toastr.success("Meetup modifié");
  }
});
