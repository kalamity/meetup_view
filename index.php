<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!--Style Link (font,css)-->
    <link rel="stylesheet" href="assets/style.css">
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
    <!--Bootstrap Script-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
    <!--JS-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!--Toastr-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet" />
    <title>Meetup</title>
</head>

<body>
    <h1 class="text-center display-4">Meetup</h1>
    <?php include('parts/nav.php') ?>
    <!--Affichage des datas-->
    <div class="row">
        <div class="col-4">
            <!--Meetups-->
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Les meetups</h5>
                    <ul class="meetups card-text">
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-8">
            <!--Un Meetups-->
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Détail du meetup</h5>
                    <div id="meetup" class="details">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Les Formulaires-->
    <div class="row justify-content-center">
        <!-- Ajouter Meetup-->
        <div class="col-4">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title text-center">Ajouter un meetup</h5>
                    <form class="form_meetup" novalidate>
                        <div class="form-group">
                            <label>Nom du Meetup</label>
                            <input type="text" class="form-control check" id="title" name="title" required>
                        </div>
                        <div class="form-group">
                                <label>Speaker</label>
                                <select class="speaker form-control" id="speaker" name="speaker" required>
                                </select>
                                
                            </div>
                        <div class="form-group">
                            <label>Description du Meetup</label>
                            <textarea class="form-control check" id="description" name="description" rows="2" required></textarea>

                        </div>
                        <div class="form-group">
                            <label>Lieu</label>
                            <select class="location form-control" id="location" name="location" required>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Lien vers l'image</label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon3">https://example.com/image.png</span>
                                </div>
                                <input type="text" class="form-control check" id="image" name="image" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Date</label>
                            <input type="date" id="date" name="date" class="form-control" required>
                        </div>
                        <button type="submit" class="btn btn-primary">Envoyer</button>
                    </form>
                </div>
            </div>
        </div>
        <!-- Modifier Meetup-->
        <div class="col-4">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title text-center"> Modifier un meetup</h5>
                    <form class="form_meetupAdd" novalidate>
                        <div class="form-group">
                            <label>Nom du Meetup</label>
                            <input type="hidden" class="form-control " id="id_up" name="id_up" required>
                            <input type="text" class="form-control checkUpdate " id="title_up" name="title_up" required>
                        </div>
                        <div class="form-group">
                                <label>Speaker</label>
                                <select class="speaker form-control" id="speaker_up" name="speaker_up" required>
                                </select>
                                
                            </div>
                        <div class="form-group">
                            <label>Description du Meetup</label>
                            <textarea class="form-control checkUpdate" name="description_up" id="description_up" rows="2" required></textarea>
                        </div>
                        <div class="form-group">
                            <label>Lieu</label>
                            <select class="location form-control" id="location_up" name="location_up" required>
                            </select>
                            
                        </div>
                        <div class="form-group">
                            <label>Lien vers l'image</label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon3">https://example.com/image.png</span>
                                </div>
                                <input type="text" class="form-control" id="image_up" name="image_up" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Date</label>
                            <input type="date" id="date_up" name="date_up" class="form-control">
                        </div>
                        <button type="submit" class="btn btn-primary update" id="add">Envoyer</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script src="assets/ajax_meetup.js"></script>
    </script>
</body>

</html>